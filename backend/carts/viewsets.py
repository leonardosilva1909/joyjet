from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    ListModelMixin
)

from carts.models import Cart
from carts.serializers import (
    Level1Serializer, Level2Serializer,
    Level3Serializer
)


class Level1ViewSet(GenericViewSet, ListModelMixin):
    serializer_class = Level1Serializer

    def get_queryset(self):
        return Cart.objects.prefetch_related(
            'cartarticles_cart__article'
        ).all()


class Level2ViewSet(GenericViewSet, ListModelMixin):
    serializer_class = Level2Serializer

    def get_queryset(self):
        return Cart.objects.prefetch_related(
            'cartarticles_cart__article'
        ).all()


class Level3ViewSet(GenericViewSet, ListModelMixin):
    serializer_class = Level3Serializer

    def get_queryset(self):
        return Cart.objects.prefetch_related(
            'cartarticles_cart__article__discounts_article'
        ).all()
