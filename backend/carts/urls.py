from django.conf.urls import include, url
from rest_framework import routers

from carts.viewsets import (
    Level1ViewSet, Level2ViewSet,
    Level3ViewSet
)
router = routers.DefaultRouter()

router.register(
    'levelone',
    Level1ViewSet,
    basename='level1'
)
router.register(
    'leveltwo',
    Level2ViewSet,
    basename='level2'
)
router.register(
    'levelthree',
    Level3ViewSet,
    basename='level2'
)

urlpatterns = [
    url('', include(router.urls)),
]
