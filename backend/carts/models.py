from django.db import models


class Article(models.Model):
    name = models.CharField(
        max_length=50,
        db_column='name'
    )
    price = models.IntegerField(
        db_column='price'
    )

    class Meta:
        db_table = 'Article'

    def __str__(self):
        return f'{self.name}'


class Cart(models.Model):
    itens = models.ManyToManyField(
        Article,
        related_name='carts_itens',
        through='CartArticles'
    )

    class Meta:
        db_table = 'Cart'

    def __str__(self):
        return f'{self.id}'


class CartArticles(models.Model):
    cart = models.ForeignKey(
        Cart, on_delete=models.DO_NOTHING,
        related_name='cartarticles_cart',
        db_column='cart'
    )
    article = models.ForeignKey(
        Article, on_delete=models.DO_NOTHING,
        related_name='cartarticles_article',
        db_column='article'
    )
    quantity = models.IntegerField(
        db_column='quantity'
    )

    class Meta:
        db_table = 'Cart_Articles'

    def __str__(self):
        return f'{self.id}'


class Discounts(models.Model):
    article = models.ForeignKey(
        Article, on_delete=models.DO_NOTHING,
        related_name='discounts_article',
        db_column='article'
    )
    type_discount = models.CharField(
        max_length=50,
        db_column='type'
    )
    value = models.IntegerField(
        db_column='value'
    )

    class Meta:
        db_table = 'Discounts'

    def __str__(self):
        return f'{self.id}'
