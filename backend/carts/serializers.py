from rest_framework import serializers


class Level1Serializer(serializers.Serializer):

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'total': sum(
                [
                    (obj.quantity * obj.article.price)
                    for obj in instance._prefetched_objects_cache[
                        'cartarticles_cart'
                    ]
                ]
            )
        }


class Level2Serializer(serializers.Serializer):

    def to_representation(self, instance):
        total = sum(
                [
                    (obj.quantity * obj.article.price)
                    for obj in instance._prefetched_objects_cache[
                        'cartarticles_cart'
                    ]
                ]
            )

        if total < 1000:
            total += 800
        elif total >= 100 and total < 2000:
            total += 400
        return {
            'id': instance.id,
            'total': total
        }


class Level3Serializer(serializers.Serializer):

    def to_representation(self, instance):
        total = []
        for obj in instance._prefetched_objects_cache['cartarticles_cart']:
            discount = obj.article._prefetched_objects_cache['discounts_article']
            if discount:
                if discount[0].type_discount == 'amount':
                    total.append(
                        (obj.quantity * (obj.article.price - discount[0].value))
                    )
                else:
                    total.append(
                        (obj.quantity * (obj.article.price - (obj.article.price * (discount[0].value / 100))))
                    )
            else:
                total.append(
                    (obj.quantity * obj.article.price)
                )

        total = sum(total)

        if total < 1000:
            total += 800
        elif total >= 100 and total < 2000:
            total += 400
        return {
            'id': instance.id,
            'total': int(total)
        }